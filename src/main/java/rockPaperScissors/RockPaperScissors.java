package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        Boolean running = true;

        while(running){
            System.out.printf("Let's play round %d" +"\n", roundCounter);
            String humanChoice = checkInput(rpsChoices);


        //Generating computer choice
            String computerChoice = computerChoice();
            String result = resultCalc(humanChoice, computerChoice);

            System.out.printf("Human chose %s, computer chose %s. %s!\n", humanChoice, computerChoice, result);
            System.out.printf("Score: human %d, computer %d\n", humanScore, computerScore);

            String cont = readInput("Do you wish to continue playing? (y/n)?");

            if (cont.equals("n")){
                running = false;
            }
            roundCounter += 1;
        
        }

        System.out.println("Bye bye :)");


    }

    public String checkInput(List<String> possibleChoices){
        String humanChoice = readInput("Your choice (Rock/Paper/Scissors)?");
        humanChoice = humanChoice.toLowerCase();
        Boolean valid = possibleChoices.contains(humanChoice);


        while(valid == false){
            System.out.printf("I do not understand %s. Could you try again?", humanChoice);
            humanChoice = readInput("\nYour choice (Rock/Paper/Scissors)?");
            humanChoice = humanChoice.toLowerCase();
            if (possibleChoices.contains(humanChoice)){
                valid = true;
            }
        }
        
        return humanChoice;
    }


    //Function to calculate computers choice based on random integers
    public String computerChoice() {
        String choice;
        Random ran = new Random();
        int randInt = ran.nextInt(3);
        if (randInt == 0){
            choice = "rock";
        }
        else if (randInt == 1){
            choice = "paper";
        }else{
            choice = "scissors";
        }

        return choice;
    }

    public String resultCalc(String humanChoice, String computerChoice) {
        String result = "";

        //Checking if the game is a tie
        if (humanChoice.equals(computerChoice)){
            result = "It's a tie";
        }

        //Checking if human or computer won the round
        else if(humanChoice.equals("rock")){
            if (computerChoice.equals("paper")){
                computerScore++;
                result = "Computer wins";
            }else{
                humanScore++;
                result = "Human wins";
            }
        }else if (humanChoice.equals("paper")){
            if (computerChoice.equals("rock")){
                humanScore++;
                result = "Human wins";
            }else{
                computerScore++;
                result = "Computer wins";
            }
        }else{
            if (computerChoice.equals("rock")){
                computerScore++;
                result = "Computer wins";
            }else{
                humanScore++;
                result = "Human wins";
            }
        }

        return result;
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}